package seiot.main.mds;

import seiot.main.devices.*;

public class SmartRadarSystem {

	public static void main(String[] args) {
		
		Light led = new seiot.main.devices.emu.Led(23);
		Light ledDetected = new seiot.main.devices.emu.Led(18);
		Light ledTracking = new seiot.main.devices.emu.Led(19);

		Button btnOn = new seiot.main.devices.p4j_impl.Button(12);
		Button btnOff = new seiot.main.devices.p4j_impl.Button(5);

		Serial serialDev = new seiot.main.devices.emu.SerialImpl(8, 9);
		
		SmartRadar smartRadar = new SmartRadar(led, ledDetected, ledTracking, btnOn, btnOff, serialDev);
		smartRadar.start();
	}

}
