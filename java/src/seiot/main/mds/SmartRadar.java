package seiot.main.mds;

import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import com.sun.media.sound.SimpleSoundbank;
import com.sun.org.apache.xpath.internal.operations.And;

import seiot.main.common.*;
import seiot.main.devices.*;

public class SmartRadar extends BasicFSMController {

	private enum State {IDLE, SCANNING, TRACKING}; 
	private State state;
	private Light ledOn;
	private Light ledDetected;
	private Light ledTracking;
	private Button btnOn;
	private Button btnOff;
	private Serial serial;
	private final static int DIST_MIN = 20;
	private final static int DIST_MAX = 300;
	private int objCounter = 0;
	private int pos = 90;
	private int rotation = 0; //0 orario, 1 antiorario
	private int distance = 0;
	/*
	//private int countDetected = 0;
	//private int countAlarms = 0;
	private static final int DT_DETECTED = 20; 
	private static final int DT_ALARM = 20; 
	*/
	
	public SmartRadar(Light ledOn, Light ledDetected, Light ledTracking, Button btnOn, Button btnOff, Serial serialDev){
		super(50);
		this.ledOn = ledOn;
		this.ledDetected = ledDetected;
		this.ledTracking = ledTracking;
		this.btnOn = btnOn;
		this.btnOff = btnOff;
		this.serial = serialDev;
		state = State.IDLE;
	}
	
	@Override
	protected void tick() {
		
		try { 	 
			switch (state){
		    case IDLE:
		    	pos = 90;
		    	serial.sendMsg(""+pos);
		      if (btnOn.isPressed()){
		    	  serial.sendMsg("Bottone premuto");
		        ledOn.switchOn();
		        state = State.SCANNING; 
		        log("SCANNING");
		      }
		      break;
		    case SCANNING:
			
			try {
				distance = Integer.parseInt(serial.readMsg());
			}
			catch (Exception e){
				distance = DIST_MAX;
			}
		      if (distance > DIST_MIN && !btnOff.isPressed()){
		    	  if(rotation==0) {
		    		  pos++;
		    		  serial.sendMsg("" + pos);
		    		  if(pos==180) {
		    			  rotation = 1;
		    			  System.out.println("Num of objects detected: " + objCounter);
		    			  objCounter = 0;
		    		  }
		    	  }
		    	  if(rotation==1) {
		    		  pos--;
		    		  serial.sendMsg(""+ pos);
		    		  if(pos==0) {
		    			  rotation = 0;
		    			  System.out.println("Num of objects detected: " + objCounter);
		    			  objCounter = 0;
		    		  }
		    	  }
		    	  if(distance < DIST_MAX) {
		    		  objCounter++;
		    		  ledDetected.switchOn();
		    		  TimeUnit.MILLISECONDS.sleep(10);
		    		  ledDetected.switchOff();
		    	  }
		        state = State.SCANNING;
			  } else if(btnOff.isPressed()){
				  ledOn.switchOff();
				  ledDetected.switchOff();
				  ledTracking.switchOff();
				  pos = 90;
	    		  serial.sendMsg("" + pos);
				  //state = State.IDLE;
		      } else {
		    	  state = State.TRACKING;
		      }
		      break;
		    case TRACKING:
		    	ledTracking.switchOn();
		    	System.out.println("Time " + /*time*/ " - Object tracked at angle" + pos + "Distance " + serial.readMsg());
		    	if(distance > DIST_MIN) {
		    		ledTracking.switchOff();
		    		state = State.SCANNING;
		    	}
		    	break;

			}
		} catch (Exception ex){
			ex.printStackTrace();
		}		
	}

	private void log(String msg){
		System.out.println("[SmartRadar] "+msg);
	}
	
}
