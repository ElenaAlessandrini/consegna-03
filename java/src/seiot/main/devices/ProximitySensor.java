package seiot.main.devices;

public interface ProximitySensor {

	boolean isObjDetected();
	
	double getObjDistance();
	
}
