package seiot.main.devices;

public interface MotionDetectorSensor {
	boolean detected();
}
