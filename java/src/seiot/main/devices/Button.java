package seiot.main.devices;

public interface Button {
	
	boolean isPressed();

}
